def factors(number):
    # ==============
    # Your code here
    factors = []
    for n in range(1, number + 1):
        if number % n == 0 and n != 1 and n != number:
            factors.append(n)
    if len(factors) != 0:
        return factors
    else:
        return(str(number) + " is a prime number")
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
