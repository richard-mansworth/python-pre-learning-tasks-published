def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == '+':
        addition = bin(a + b)
        return str(addition[2:])
    elif operator == '-':
        subtract = bin(a - b)
        return str(subtract[2:])
    elif operator == '*':
        multiply = bin(a * b)
        return str(multiply[2:])
    elif operator == '/':
        divide = bin(int(a / b))
        return str(divide[2:])
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
